# pf2e-b1-german

Öffentliches Projekt des MHB1 German von Ulisses Spiele

## Fehler melden

Falls du Fehler im Foundry-Modul Monsterhandbuch 1 feststellst, melden diese bitte in diesem Projekt unter dem Punkt Issues.
Gib dazu bitte folgende Informationen an:

- Names des Monsters
- Name der Fähigkeit oder des Gegenstands
- Was ist falsch?
- Wie müsste es richtig sein/was erwartest du?
